import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "2a9fb76ad675e44e13618281785ab977";
  private IMP = "units=metric"


  constructor(private http:HttpClient,private db:AngularFirestore) { }

  citiesCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  

  addWeather(userId:string,name:string,temp:number,humidity:number,result:string){
    const weather = {name:name,temp:temp,humidity:humidity,result:result}; 
    this.userCollection.doc(userId).collection('cities').add(weather);
  }
  
  public getweather(userId){
    this.citiesCollection = this.db.collection(`users/${userId}/cities`); 
    return this.citiesCollection.snapshotChanges();
} 


  searchWetherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }
  
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server eroor');
  }
  
  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp, 
      humidity:data.main.humidity,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
  
}
