import { AuthService } from './../auth.service';
import { Weather } from './../interfaces/weather';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PredictService } from '../predict.service';

export interface PeriodicElement {
  position:number;
  name: string;
  temp:number;
  humidity:number
}

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  cities:any[] = [
    {position: 1, name: 'London', temp:25, humidity:80},
    {position: 2, name: 'Paris',temp:10, humidity:50},
    {position: 3, name: 'Tel Aviv',temp:10, humidity:25},
    {position: 4, name: 'Jerusalem',temp:5, humidity:90},
    {position: 5, name: 'Berlin',temp:0, humidity:40},
    {position: 6, name: 'Rome',temp:30, humidity:35},
    {position: 7, name: 'Dubai',temp:29, humidity:29},
    {position: 8, name: 'Athens',temp:11, humidity:27},
  ];  

  displayedColumns: string[] = ['position', 'name','data','temp','humidity','icon','predict','result'];

  city:string; 
  name:string; 
  temp:number; 
  humidity:number;
  image:string; 
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  result:string;
  userId:string;
  cities$;
  
  add(name:string, temp:number, humidity:number, result:string){
    this.weatherService.addWeather(this.userId,name,temp,humidity,result); 
  }


  predict(i){
    this.predictServise.predict(this.cities[i].temp,this.cities[i].humidity).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Yes Rain';
          console.log(result)
        } else {
          var result = 'No Rain';
          console.log(result)
        }
        this.cities[i].result = result      }
    );  
  }


  getDate(i){
    this.weatherData$ = this.weatherService.searchWetherData(this.cities[i].name);
    this.weatherData$.subscribe(
      data => {
        data.temperature = data.temperature;
        data.image= data.image; 
        data.humidity = data.humidity;
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )

  }


  constructor(private weatherService:WeatherService,private predictServise:PredictService,private authService:AuthService) { }

  ngOnInit(): void {
//     this.authService.getUser().subscribe(
//       user => {
//         this.userId = user.uid;
//         console.log(this.userId); 
//         this.weatherData$ = this.weatherData$.(this.userId);
//         this.weatherData$.subscribe(
//           docs =>{
//             this.cities = [];
//             for(let document of docs){
//               const city:any = document.payload.doc.data();

//               city.id = document.payload.doc.id; 
//               this.cities.push(city); 
//             }
//           }
//         ) 

//         }
//       )
  }
}


