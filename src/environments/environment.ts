// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBhVJYdv9QY_Xa0BkEjmgHgngXLUC6RBl8",
    authDomain: "testb-12113.firebaseapp.com",
    projectId: "testb-12113",
    storageBucket: "testb-12113.appspot.com",
    messagingSenderId: "310835999540",
    appId: "1:310835999540:web:8b2712829123fc423feb61"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
